#!/bin/bash
#
# Aŭtorrajto © 2022 - 2023 Arturo Teslao
# Ĉi tiu verko estas disponebla laŭ la permesilo GNUa GPLv2, Ĝenerala Publika Permesilo de GNU Tutmonda versio 2.
# https://www.gnu.org/licenses/gpl-2.0.html
#
#
echo -e "\nElektu la direkton de la traduko:\n1 - Esperanto-angla.\n2 - Angla-Esperanto.\n3 - Fermi aplikaĵon.\n"
while read varDirekto
  do
    case $varDirekto in
      1) echo -e "\nSkribu tekston:"
         read varTeksto
         echo $varTeksto | apertium -a eo-en
         echo -e "\nElektu la direkton de la traduko:\n1 - Esperanto-angla.\n2 - Angla-Esperanto.\n3 - Fermi aplikaĵon.\n"
      ;;
      2) echo -e "\nSkribu tekston:"
         read varTeksto
         echo $varTeksto | apertium -a en-eo
         echo -e "\nElektu la direkton de la traduko:\n1 - Esperanto-angla.\n2 - Angla-Esperanto.\n3 - Fermi aplikaĵon.\n"
      ;;
      3) exit
      ;;
      *) echo -e "\nMalĝusta enigo!\nElektu la direkton de la traduko:\n1 - Esperanto-angla.\n2 - Angla-Esperanto.\n3 - Fermi aplikaĵon.\n"
      ;;
    esac
  done
#
#
# Copyright © 2022 - 2023 Arthur Tesla
# This work is licensed under a GNU GPLv2, GNU General Public License (GNU GPLv2).
# https://www.gnu.org/licenses/gpl-2.0.html
