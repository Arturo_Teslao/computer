#!/bin/bash
# "Debian GNU/Linux" ("RISC-V")
# ĉefuzanto@komputilo:~#
# Tempo
timedatectl set-timezone UTC
timedatectl set-local-rtc 0
# "GRUB"
# sed 's/set menu_color_normal=COLOR\/blue/set menu_color_normal=dark-gray\/black' /etc/grub.d/05_debian_theme
# sed 's/set menu_color_highlight=COLOR\/blue/set menu_color-highlight=yellow\/black' /etc/grub.d/05_debian_theme
# Pakaĵoj
echo "## Main:" > /etc/apt/sources.list
echo "deb https://deb.debian.org/debian/ bookworm main" >> /etc/apt/sources.list
echo "deb-src https://deb.debian.org/debian/ bookworm main" >> /etc/apt/sources.list
echo "## Security:" >> /etc/apt/sources.list
echo "deb https://security.debian.org/debian-security bookworm-security main" >> /etc/apt/sources.list
echo "deb-src https://security.debian.org/debian-security bookworm-security main" >> /etc/apt/sources.list
echo "## Updates:" >> /etc/apt/sources.list
echo "deb https://deb.debian.org/debian/ bookworm-updates main" >> /etc/apt/sources.list
echo "deb-src https://deb.debian.org/debian/ bookworm-updates main" >> /etc/apt/sources.list
echo "## Backports:" >> /etc/apt/sources.list
echo "# deb https://deb.debian.org/debian/ bookworm-backports main" >> /etc/apt/sources.list
echo "# deb-src https://deb.debian.org/debian/ bookworm-backports main" >> /etc/apt/sources.list
echo "## Tor:" >> /etc/apt/sources.list
echo "#deb tor+https://XXXXXXXXXXXXXXXX.onion/debian/ bookworm main" >> /etc/apt/sources.list
echo "#deb-src tor+https://XXXXXXXXXXXXXXXX.onion/debian/ bookworm main" >> /etc/apt/sources.list
echo "## dbgsym:" >> /etc/apt/sources.list
echo "# deb https://deb.debian.org/debian-debug/ bookworm-debug main" >> /etc/apt/sources.list
apt-get update -y
apt-get install aptitude -y
# Konsolo
aptitude install vlock -y
# Sistemo
aptitude install htop memtest86+ i2c-tools smartmontools evtest -y
# Arĥivoj
aptitude install gpg tar gzip bzip2 xz-utils zpaq p7zip -y
# Dosieroj
aptitude install mc -y
# Lingvoj
aptitude install aspell aspell-eo aspell-en -y
# Klavaro
dpkg-reconfigure locales
dpkg-reconfigure keyboard-configuration
service keyboard-setup restart
dpkg-reconfigure fontconfig-config
dpkg-reconfigure console-setup
systemctl restart console-setup.service
# Lumo de ekranbloko
aptitude install light -y
# Restart system
systemctl reboot
